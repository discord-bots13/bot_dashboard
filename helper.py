import glob
import json
import modeles
import subprocess

PATH_BOTS = '/home/ubuntu/bots'
DELIMITER = '/'

def list_bots():
    list_path = glob.glob(PATH_BOTS + DELIMITER + 'bot_*')
    return list(map(lambda x:x.split(DELIMITER)[-1],list_path))

def get_logs(name):
    filepath = PATH_BOTS + DELIMITER + name + DELIMITER + name + '.log'
    if glob.glob(filepath):
        with open(filepath, 'r', encoding='utf-8') as f:
            return ''.join(f.readlines()[-25:])

    return ''

def get_status(name):
    command = subprocess.Popen(["tmux", "ls"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output, errors = command.communicate()
    command.wait()
    lines = str(output).split('\\n')
    
    for line in lines:
        if line.split(':')[0] == name:
            return 'on'
    return 'off'

def get_infos(name):
    with open(PATH_BOTS + DELIMITER + name + DELIMITER + 'infos.json', 'r', encoding='utf-8') as f:
        data = json.load(f)
    return modeles.get_first_infos(data)

def is_bot_exist(name):
    return name in [bot for bot in list_bots()]

def get_all_infos(name):
    with open(PATH_BOTS + DELIMITER + name + DELIMITER + 'infos.json', 'r', encoding='utf-8') as f:
        data = json.load(f)
    infos = modeles.get_all_infos(data)
    infos.logs = get_logs(name)
    infos.status = get_status(name)

    return infos

if __name__ == '__main__':
    name = 'bot_cavax'
    print(get_status(name))
